Hola Amigos de Elenas.

Soy Luis Bolívar, quién desarrollo está prueba cool.
Les voy a explicar como ejecutar esta belleza, y ciertas consideraciones al respecto del proyecto.

En caso de emergencia dirijase a mi correo personal [lebolivarc@gmail.com]
Este aplicativo también está en GitLab: [https://gitlab.com/TheLuis/oct19_elenasprocess.git]

[Lenguaje]: Java (JDK 1.8)

[Pasos para Ejecución]:
1. Ubicar el archivo(.jar) en el escritorio de la maquina donde será ejecutado.
2. Crear una carpeta con el nombre (result) en minúsculas.
3. Guardar el archivo base (orders.csv) dentro de la carpeta (result)
4. Dejar el archivo ejecutable (Oct19_ElenasProcess.jar) en el escritorio. (sí, por fuera de la carpeta result, sólo en el escritorio)
4. Abrir la línea de comando de windows o linux y ejecutar la consola como administrador o superusuario.
5. Ubicarse en la ruta del ecritorio dentro de la consola (cd cd:\user\xxx\Desktop\)
6. Ingresar las siguientes instrucciones: java -jar Oct19_ElenasProcess.jar
7. contemplarlo.

[Consideraciones]:
1. Tener instalado java JRE o JDK, en su versión 8, y tener las variables de entorno configuradas.
	1.1. Para asegurarse de que todo marcha sobre ruedas ejecuten por línea de comando y escriban
		las siguientes instrucciones: (java -version) es una buena de asegurarse de que todo está cool.

2. No use librerías extras, tengo un pom.xml, porque construí el proyecto en caso de usar líbrerias externas
	pero al final no fue necesario.

3. No use inyección de dependencias, simplemente emule servicios por medio de clases (instanciadas) 
	en el main(CoreProces.class) :).

4. No use librerias de log (ejemplo: Log4j) use solo escritura sobre la consola del sistema (mala practica)
	por lo general hago todo sobre logs, pero quería evitar dependencias :)).

5. Está medio documentado, porqué ajá, sé que lo van a leer, siento que es fácil de navegar y de entender, 
	y si o es así, espero sus Feedbacks.