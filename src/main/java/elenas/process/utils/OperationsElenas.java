package elenas.process.utils;

import elenas.process.models.Order;

/**
 * Clase utilitaria para hacer los calulos de los archivos del Elenas.
 * 
 * @author TheLuis
 * @category Utilitarios
 */
public class OperationsElenas {

	/**
	 * Operaci�n que calcula la comisi�n de la embajadora
	 * 
	 * @param comissionAmbassador total de embajadora acumulado
	 * @param order               orden a evaluar
	 * @return sumatoria de todas las comisiones de la embajadora
	 */
	public static Double comissionAmbassador(Double comissionAmbassador, Order order) {

		Double comission = new Double(
				(comissionAmbassador.equals(null) || comissionAmbassador < 0) ? 0 : comissionAmbassador);

		comission += (order.getPrice() * order.getSellerComissionPercent());

		return comission;
	}

	/**
	 * Operaci�n que c�lcula el total ingresos de la marca
	 * 
	 * @param totalRevenue total de la marca acumulado
	 * @param order        orden de compra a evaluar
	 * @return sumatoria de los ingresos de la marca
	 */
	public static Double totalRevenue(Double totalRevenue, Order order) {
		Double revenue = new Double((totalRevenue.equals(null) || totalRevenue < 0) ? 0 : totalRevenue);

		Double comissionAmbassadorOrder = comissionAmbassador(0D, order);
		Double comissionElenas = comissionElenas(order.getPrice());
		Double logisticCost = Double.valueOf(
				(order.getLogisticCost().equals(null) || order.getLogisticCost() < 0) ? 0 : order.getLogisticCost());

		Double price = Double.valueOf((order.getPrice().equals(null) || order.getPrice() < 0) ? 0 : order.getPrice());

		revenue += (price - comissionAmbassadorOrder - comissionElenas - logisticCost);

		return revenue;
	}

	/**
	 * Operaci�n que c�lcula la comission de Elenas
	 * 
	 * @param price precio a c�lcular
	 * @return comisi�n de Elenas
	 */
	private static Double comissionElenas(Double price) {
		Double priceTotal = new Double((price.equals(null) || price < 0) ? 0 : price);
		Double comissionElenas = priceTotal * ConstantsElenas.PORCENTAJE_ELENAS;

		return comissionElenas;

	}
}
