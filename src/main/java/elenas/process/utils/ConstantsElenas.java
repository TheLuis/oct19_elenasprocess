package elenas.process.utils;

public class ConstantsElenas {

	public static final String HOLI = "";

	public static final String RUTA_ARCHIVOS = "result";

	public static final String RUTA_ORDERS = "orders.csv";

	public static final String DELIMITADOR_CSV_COMA = ",";

	public static final String NOMBRE_ARCHIVO_EMBAJADORAS = "ambassadors.csv";

	public static final String NOMBRE_ARCHIVO_MARCAS = "brands.csv";

	public static final String CABECERA_EMBAJADORAS = "fileEmbajadoras";

	public static final String CABECERA_MARCAS = "fileMarcas";

	public static final String CABECERA_1_EMBAJADORAS = "Seller_id";

	public static final String CABECERA_2_EMBAJADORAS = "Total_comission";

	public static final String CABECERA_1_MARCAS = "Brand_name";

	public static final String CABECERA_2_MARCAS = "Brand_name";

	public static final Double PORCENTAJE_ELENAS = 0.12D;

}
