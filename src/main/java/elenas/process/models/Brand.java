package elenas.process.models;

/**
 * Clase que representa la información de una marca
 * 
 * @author TheLuis
 * @category Modelo
 */
public class Brand {

	/**
	 * Nombre de la marca
	 */
	private String name;

	/**
	 * total de ingresos de la marca
	 */
	private Double totalRevenue;

	public Brand(String name, Double totalRevenue) {
		this.name = name;
		this.totalRevenue = totalRevenue;
	}

	public Brand() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getTotalRevenue() {
		return totalRevenue;
	}

	public void setTotalRevenue(Double totalRevenue) {
		this.totalRevenue = totalRevenue;
	}

	@Override
	public String toString() {
		return "BrandService [name=" + name + ", totalRevenue=" + totalRevenue + "]";
	}
}
