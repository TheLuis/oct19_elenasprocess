package elenas.process.models;

/**
 * Clase que representa una embajadora(Vendedora)
 * 
 * @author TheLuis
 * @category Modelo
 */
public class Ambassadors {

	/**
	 * Identificación de una embajadora(Vendedora)
	 */
	private Integer id;

	/**
	 * Comisión total de la embajadora(Vendedora)
	 */
	private Double totalComission;

	public Ambassadors(Integer id, Double totalComission) {
		this.id = id;
		this.totalComission = totalComission;
	}

	public Ambassadors() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getTotalComission() {
		return totalComission;
	}

	public void setTotalComission(Double totalComission) {
		this.totalComission = totalComission;
	}

	@Override
	public String toString() {
		return "Ambassadors [id=" + id + ", totalComission=" + totalComission + "]";
	}

}
