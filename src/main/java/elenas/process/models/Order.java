package elenas.process.models;

/**
 * Clase que representa una orden
 * 
 * @author TheLuis
 * @category Modelo
 */
public class Order {

	/**
	 * Numero de orden de la venta
	 */
	private Integer orderNumber;

	/**
	 * Respresentación del nombre de la marca
	 */
	private String brandName;

	/**
	 * Nombre del producto
	 */
	private String productName;

	/**
	 * Precio del producto
	 */
	private Double price;

	/**
	 * cantidad del producto
	 */
	private Integer quantity;

	/**
	 * Descuento total del producto
	 */
	private Double totalDiscount;

	/**
	 * Porcentaje de comisión de la embajadora(vendedora)
	 */
	private Double sellerComissionPercent;

	/**
	 * Costo de la logistica
	 */
	private Integer logisticCost;

	/**
	 * Identificador de la ebajadora(vendedora)
	 */
	private Integer sellerID;

	/**
	 * 
	 * @param orderNumber
	 * @param brandName
	 * @param productName
	 * @param price
	 * @param quantity
	 * @param totalDiscount
	 * @param sellerComissionPercent
	 * @param logisticCost
	 * @param sellerID
	 */
	public Order(Integer orderNumber, String brandName, String productName, Double price, Integer quantity,
			Double totalDiscount, Double sellerComissionPercent, Integer logisticCost, Integer sellerID) {
		this.orderNumber = orderNumber;
		this.brandName = brandName;
		this.productName = productName;
		this.price = price;
		this.quantity = quantity;
		this.totalDiscount = totalDiscount;
		this.sellerComissionPercent = sellerComissionPercent;
		this.logisticCost = logisticCost;
		this.sellerID = sellerID;
	}

	public Order() {

	}

	public Integer getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getTotalDiscount() {
		return totalDiscount;
	}

	public void setTotalDiscount(Double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}

	public Double getSellerComissionPercent() {
		return sellerComissionPercent;
	}

	public void setSellerComissionPercent(Double sellerComissionPercent) {
		this.sellerComissionPercent = sellerComissionPercent;
	}

	public Integer getLogisticCost() {
		return logisticCost;
	}

	public void setLogisticCost(Integer logisticCost) {
		this.logisticCost = logisticCost;
	}

	public Integer getSellerID() {
		return sellerID;
	}

	public void setSellerID(Integer sellerID) {
		this.sellerID = sellerID;
	}

	@Override
	public String toString() {
		return "Order [orderNumber=" + orderNumber + ", brandName=" + brandName + ", productName=" + productName
				+ ", price=" + price + ", quantity=" + quantity + ", totalDiscount=" + totalDiscount
				+ ", sellerComissionPercent=" + sellerComissionPercent + ", logisticCost=" + logisticCost
				+ ", sellerID=" + sellerID + "]";
	}

}
