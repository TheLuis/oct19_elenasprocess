package elenas.process.servicies;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import elenas.process.models.Order;
import elenas.process.utils.OperationsElenas;

/**
 * Clase que representa los servicios para la construccion de las cuentas de la
 * marca
 * 
 * @author TheLuis
 * @category Servicio
 */
public class BrandService {

	public Map<String, Double> getBrandTotalRevenue(List<Order> listOrders) throws Exception {

		System.out.println("[getBrandTotalRevenue]: Realizando los calculos de nuestras marcas");
		Map<String, Double> mapBrandTotalRevenue = new TreeMap<String, Double>();

		/** Validando la lista de ordenes */
		if (listOrders.size() == 0) {
			System.err.println("[getBrandTotalRevenue]: La lista de ordenes est� vacia");
			throw new Exception("[getBrandTotalRevenue]: La lista de ordenes est� vacia");
		}

		try {
			/** construyendo informaci�n para archivo csv */
			for (Order orderIndex : listOrders) {
				Double totalRevenue = new Double(0);

				/** Validando existencia de embajadoras */
				if (!mapBrandTotalRevenue.containsKey(orderIndex.getBrandName())) {
					mapBrandTotalRevenue.put(orderIndex.getBrandName(), totalRevenue);
				} else {
					totalRevenue = mapBrandTotalRevenue.get(orderIndex.getBrandName());
				}

				totalRevenue = OperationsElenas.totalRevenue(totalRevenue, orderIndex);
				mapBrandTotalRevenue.put(orderIndex.getBrandName(), totalRevenue);
			}

			System.out.println("[getBrandTotalRevenue]: Mapa de datos de las marcas se ha creado exitosamente.");
			System.out.println("[getBrandTotalRevenue]: Mapa de datos: [" + mapBrandTotalRevenue + "].");

		} catch (Exception exception) {
			System.err.println("[getBrandTotalRevenue]: No se pudieron c�lcular los ingresos de las marcas.");
			throw exception;
		}

		return mapBrandTotalRevenue;
	}
}
