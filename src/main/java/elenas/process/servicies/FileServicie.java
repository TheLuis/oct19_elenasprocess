package elenas.process.servicies;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import elenas.process.models.Order;
import elenas.process.utils.ConstantsElenas;

public class FileServicie {

	// private String pathOfCsv = ConstantsElenas.RUTA_ORDERS;

	/**
	 * Metodo que lee un archivo y devuelve ordenes
	 * 
	 * @return una lista de ordenees
	 * @throws Exception
	 */
	public List<Order> getOrdersByFile() throws Exception {

		System.out.println("[getOrdersByFile]: Empezando a obtener las ordenes");
		List<Order> listOrders = new ArrayList<Order>();

		try {

			Path root = FileSystems.getDefault().getPath("").toAbsolutePath();
			Path filePath = Paths.get(root.toString(), ConstantsElenas.RUTA_ARCHIVOS, ConstantsElenas.RUTA_ORDERS);

			System.out.println("[getOrdersByFile]: Ruta: [" + filePath + "].");
			BufferedReader csvReader = new BufferedReader(new FileReader(filePath.toFile()));

			String lineFileCsv;
			int index = 0;

			while ((lineFileCsv = csvReader.readLine()) != null) {
				if (index != 0) {
					String[] valuesOfOrder = lineFileCsv.split(ConstantsElenas.DELIMITADOR_CSV_COMA);
					Order order = BuildOrder(valuesOfOrder);
					listOrders.add(order);
				}
				index++;
			}
		} catch (Exception exception) {
			System.err.println("[getOrdersByFile]: No se pudo leer correctamente el archivo de las ordenes");
			throw exception;
		}

		return listOrders;
	}

	/**
	 * M�todo que construye una orden
	 * 
	 * @param valuesOfOrder lista de String con los valores de la fila de una orden
	 * @return una orden
	 * @throws Exception
	 */
	private Order BuildOrder(String[] valuesOfOrder) throws Exception {

		Order order = new Order();
		System.out.println("[BuildOrder]: Empezando a construir la orden.");

		if (valuesOfOrder.length == 0) {
			System.err.println("[BuildOrder]: La lista de valores de la orden est� vacia");
			Exception exception = new Exception("[BuildOrder]: La lista de valores de la orden est� vacia");
			throw exception;
		}

		try {
			order.setOrderNumber(Integer.valueOf(valuesOfOrder[0].toString()));
			order.setBrandName(String.valueOf(valuesOfOrder[1].toString()));
			order.setProductName(String.valueOf(valuesOfOrder[2].toString()));
			order.setPrice(Double.valueOf(valuesOfOrder[3].toString()));
			order.setQuantity(Integer.valueOf(valuesOfOrder[4].toString()));
			order.setTotalDiscount(Double.valueOf(valuesOfOrder[5].toString()));
			order.setSellerComissionPercent(Double.valueOf(valuesOfOrder[6].toString()));
			order.setLogisticCost(Integer.valueOf(valuesOfOrder[7].toString()));
			order.setSellerID(Integer.valueOf(valuesOfOrder[8].toString()));

		} catch (Exception exception) {
			System.err.println("[BuildOrder]: Alguno de los valores de la orden tiene alg�n problema.");
			throw exception;
		}

		System.out.println("[BuildOrder]: Orden construida con �xito + [" + order + "].");
		return order;
	}

	/**
	 * Metodo que crea el archivo con los datos procesados
	 * 
	 * @param nameFile   nombre del archivo ha crear
	 * @param mapHeaders cabeceras del archivo csv
	 * @param mapData    estructura de datos procesados
	 * @throws Exception
	 */
	public void BuildFile(String nameFile, Map<Integer, String> mapHeaders, Map<String, Double> mapData)
			throws Exception {

		Path root = FileSystems.getDefault().getPath("").toAbsolutePath();
		Path filePath = Paths.get(root.toString(), ConstantsElenas.RUTA_ARCHIVOS, nameFile);

		System.out.println("[BuildFile] Creando el archivo: [" + nameFile + "], en la ruta: [" + filePath + "].");

		FileWriter csvWriter = new FileWriter(filePath.toFile());
		try {
			File directory = new File(filePath.toString());

			if (!directory.exists()) {
				System.out.println("[BuildFile] Archivo creado: [" + nameFile + "]");
				directory.mkdir();
			} else {
				System.err.println("[BuildFile] El archivo ya existe" + ": [" + nameFile + "]");

				csvWriter.close();

				if (directory.delete()) {
					System.out.println("[BuildFile] Archivo borrado exitosamente: [" + nameFile + "]");
					csvWriter = new FileWriter(filePath.toFile());
					System.out.println("[BuildFile] Archivo creado: [" + nameFile + "]");
				} else {
					System.err.println("[BuildFile] Archivo borrado exitosamente: [" + nameFile + "]");
				}
				// Files.deleteIfExists(directory.toPath());
			}

			/** Construyendo las cabeceras */
			csvWriter.append(mapHeaders.get(1));
			csvWriter.append(",");
			csvWriter.append(mapHeaders.get(2));
			csvWriter.append("\n");

			for (Entry<String, Double> mapValueIndex : mapData.entrySet()) {
				csvWriter.append(mapValueIndex.getKey());
				csvWriter.append(",");
				csvWriter.append(mapValueIndex.getValue().toString());
				csvWriter.append("\n");
			}

			System.out.println("[BuildFile] El archivo ha sido creado exitosamente.");

		} catch (Exception exception) {
			System.err.println("[BuildFile] El archivo no pudo ser creado." + "\n" + exception.getStackTrace());
			throw exception;

		} finally {
			csvWriter.flush();
			csvWriter.close();
		}
	}

	/**
	 * Metodo que realiza la construccion de los headers de los archivos
	 * 
	 * @param codeHeadercodigo de acabecera que define que cabecera se creara
	 * @return un mapa de interes con el orden de las cabeceras y las cabeceras
	 * @throws Exception
	 */
	public Map<Integer, String> buidlHeaders(String codeHeader) throws Exception {
		Map<Integer, String> mapHeaders = new TreeMap<Integer, String>();

		System.out.println("[buidlHeaders]: Construyendo las cabeceras");

		if (codeHeader.toString().equals(ConstantsElenas.CABECERA_EMBAJADORAS)) {
			mapHeaders.put(1, ConstantsElenas.CABECERA_1_EMBAJADORAS);
			mapHeaders.put(2, ConstantsElenas.CABECERA_2_EMBAJADORAS);
		} else if (codeHeader.toString().equals(ConstantsElenas.CABECERA_MARCAS)) {
			mapHeaders.put(1, ConstantsElenas.CABECERA_1_MARCAS);
			mapHeaders.put(2, ConstantsElenas.CABECERA_2_MARCAS);
		} else {
			System.err.println("[buidlHeaders]: Error construyendo las cabeceras");
			throw new Exception("[buidlHeaders]: No se logro contruir las caberas del archivo: [" + codeHeader + "].");
		}

		return mapHeaders;
	}
}
