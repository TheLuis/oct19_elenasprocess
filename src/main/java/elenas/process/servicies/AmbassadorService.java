package elenas.process.servicies;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import elenas.process.models.Order;
import elenas.process.utils.OperationsElenas;

/**
 * Clase que representa los servicios para construir los calculos de las marcas
 * 
 * @author TheLuis
 * @category Servicio
 */
public class AmbassadorService {

	/**
	 * Contruye la data necesaria para crear el documento para las embajadoras
	 * 
	 * @param listOrders lista de ordenes
	 * @return un mapa de enteros, por id de laembajadora, con el resultado de la
	 *         operaci�n de las ordenes
	 * @throws Exception
	 */
	public Map<String, Double> getAmbassadorWithComission(List<Order> listOrders) throws Exception {

		System.out.println("[getAmbassadorWithComission]: Realizando los calculos de nuestras embajadoras");
		Map<String, Double> mapAmbassadorWithComission = new TreeMap<String, Double>();

		/** Validando la lista de ordenes */
		if (listOrders.size() == 0) {
			System.err.println("[getAmbassadorWithComission]: La lista de ordenes est� vacia");
			throw new Exception("[getAmbassadorWithComission]: La lista de ordenes est� vacia");
		}

		try {
			/** construyendo informaci�n para archivo csv */
			for (Order orderIndex : listOrders) {
				Double comissionAmbassador = new Double(0);

				/** Validando existencia de embajadoras */
				if (!mapAmbassadorWithComission.containsKey(orderIndex.getSellerID().toString())) {
					mapAmbassadorWithComission.put(orderIndex.getSellerID().toString(), comissionAmbassador);
				} else {
					comissionAmbassador = mapAmbassadorWithComission.get(orderIndex.getSellerID().toString());
				}

				comissionAmbassador = OperationsElenas.comissionAmbassador(comissionAmbassador, orderIndex);
				mapAmbassadorWithComission.put(orderIndex.getSellerID().toString(), comissionAmbassador);
			}

			System.out.println("[getAmbassadorWithComission]: Mapa de datos de Embajadoras creado exitosamente.");
			System.out.println("[getAmbassadorWithComission]: Mapa de datos: [" + mapAmbassadorWithComission + "].");

		} catch (Exception exception) {
			System.err.println("[getAmbassadorWithComission]: No se pudieron realizar las comisiones por embajadora.");
			throw exception;
		}

		return mapAmbassadorWithComission;
	}

}
