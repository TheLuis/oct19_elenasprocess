package elenas.process.core;

import java.util.List;
import java.util.Map;

import elenas.process.models.Order;
import elenas.process.servicies.AmbassadorService;
import elenas.process.servicies.BrandService;
import elenas.process.servicies.FileServicie;
import elenas.process.utils.ConstantsElenas;

/**
 * Clase principal donde sucederá toda la magia
 * 
 * @author TheLuis
 *
 */
public class CoreProcess {

	public static void main(String[] args) throws Exception {

		System.out.println("[CoreProcess]: Iniciando esta linda aplicación");

		/** [SERVICIOS] */
		FileServicie fileServicie = new FileServicie();
		AmbassadorService ambassadorService = new AmbassadorService();
		BrandService brandService = new BrandService();

		try {
			System.out.println("[CoreProcess]: Cargando datos de Elenas a la aplicación");
			List<Order> listOrders = fileServicie.getOrdersByFile();

			System.out.println("[CoreProcess]: Procesando los datos de Elenas");
			Map<String, Double> mapAmbassadorsComission = ambassadorService.getAmbassadorWithComission(listOrders);
			Map<String, Double> mapBrandTotalRevenue = brandService.getBrandTotalRevenue(listOrders);

			System.out.println("[CoreProcess]: Creando el archivo: [" + ConstantsElenas.CABECERA_EMBAJADORAS + "].");
			fileServicie.BuildFile(ConstantsElenas.NOMBRE_ARCHIVO_EMBAJADORAS,
					fileServicie.buidlHeaders(ConstantsElenas.CABECERA_EMBAJADORAS), mapAmbassadorsComission);

			System.out.println("[CoreProcess]: Creando el archivo: [" + ConstantsElenas.CABECERA_MARCAS + "].");
			fileServicie.BuildFile(ConstantsElenas.NOMBRE_ARCHIVO_MARCAS,
					fileServicie.buidlHeaders(ConstantsElenas.CABECERA_MARCAS), mapBrandTotalRevenue);

			System.out.println("[CoreProcess]: Control de flujo");
		} catch (Exception exception) {
			System.err
					.println("[CoreProcess]: Se ha encontrado un error durante el flujo de la aplicación, revisar logs."
							+ "\n" + exception.getStackTrace());
			throw exception;
		} finally {
			System.out.println("[CoreProcess]: Fin de ejecución de la aplicación ");
		}
	}

}
